import { APP_ID, Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { PostService } from '../../services/post.service';
import { global } from '../../services/global';
import { UserService } from '../../services/user.service';
import { User } from 'src/app/models/user';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PostService, UserService]
})
export class HomeComponent implements OnInit {
  public payPalConfig ? : IPayPalConfig;
  public page_title: string;
  public url;
  public posts: Array<Post>;
  public identity;
  public token;
  public allImages: [];
  public items: any;
  public msg: string = "";
  public display: boolean = false;
  public displayInfo: boolean = false;
  credit_image: string = "";
  total: string = "";
  total_carrito: number = 0;
  message: string = "";
  messageFromCart: boolean = false;
  modalCode: boolean = false;
  sortOrder: number = -1;
  sortField: string = "id";
  public obj = {
    codigo: ''
  };
  public cart: any[] = [];
  public showLinkToMisCompras: boolean = false;

  constructor(
    private _postService: PostService,
    private _userService: UserService,
    private spinner: NgxSpinnerService
  ) {
    this.page_title = 'Bienvenido';
    this.url = global.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

   ngOnInit() {
    this.spinner.show();
    this.initConfig();
    this.getCards();
    // la comenté porque no he creado la tabla posts ya que no va a servir para este desarrollo
    //this.getPosts();  
    this.getPayPalValue();
    this.getTotalPriceInCart();
    this.credit_image = "creditos_vuelta.jpg";
    
  }

  getTotalPriceInCart() {
    this.total_carrito = Number(localStorage.getItem('total_carrito'));
    this.total =  localStorage.getItem('total_carrito');
  }
  getCards(): void {
    this._postService.getCardsNoLogin().subscribe(
      response => {
        console.log("imageeSSSS!!");
        console.log(response);
        
        this.allImages = response;
        localStorage.setItem('cards', JSON.stringify(this.allImages));
         
        this.getCartFromLocalStorage()
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      }
    )
  }
  submitCode(form) {
    this.spinner.show();
    this._postService.redeem_code(this.token, this.obj).subscribe(
      response => {
        this.msg = "";
        if (response.code === 200) {
            this.msg = response.message;
            this.showLinkToMisCompras = true;
        }
        this.spinner.hide();
      },
      error => {
        if (error.hasOwnProperty('error')) {
          this.msg = "";
          switch(error.error.code) {
              case 400:
                  this.msg = error.error.message;
              break;

              case 404:
                this.msg = error.error.message;
              break;
          }
        }
        this.spinner.hide();
      }
    );
  }
  showModalCode() {
    this.showLinkToMisCompras = false;
    this.modalCode = true;
  }
  getPosts() {
    this._postService.getPosts().subscribe(
      response => {
        if (response.status == "success") {
          this.posts = response.posts;

        }
      },
      error => {
        console.log(error);
      }
    );
  }

  addToCart(imageData) {
    
    let continuar: Boolean = true;
    let cart = [];
    console.log(this.total_carrito);
      this.cart.find(item => {
          if(item.id === imageData.id) {
              this.msg = "Ya has agregado este producto al carrito";
              continuar = false;
              this.messageFromCart = true;
          }       
      });

      if(continuar) {
        this.cart.push(imageData);
        this.total_carrito = this.total_carrito + Number(imageData.precio);
        localStorage.setItem('cart', JSON.stringify(this.cart));
        localStorage.setItem('total_carrito', this.total_carrito.toString());
        this.total = localStorage.getItem('total_carrito');
        console.log(this.cart);
      }
    
  }

  getCartFromLocalStorage() {
     
    if (localStorage.getItem('cart') !== null) {
      this.cart = JSON.parse(localStorage.getItem('cart'));
    }
  }

  deleteFromCart(id) {
    if (this.cart.length > 1) {
        this.cart.find(item => {
           
        let card: any[] = [];
        if (item.id !== id) {
          card.push(item);
          this.cart = card;
          this.total_carrito = this.total_carrito - Number(item.precio);
          localStorage.setItem('total_carrito', this.total_carrito.toString());
          localStorage.setItem('cart', JSON.stringify(this.cart));
          this.total = localStorage.getItem('total_carrito');
        }
      })
    }
    else {
      
      this.total_carrito = 0;
      this.total = "";
      this.cart = [];
      localStorage.removeItem('total_carrito');
      localStorage.removeItem('cart');
    }
  }

   getItemList(): any[] {
    this.getCartFromLocalStorage();
    let payPalItem: any[] = [];
    let itemObj = {};
    for (const item of this.cart) {
        itemObj = {
            name: item.nombre_deck,
            quantity: 1,
            unit_amount: {
              currency_code: 'MXN',
              value: item.precio.toString(),
              deck_id: item.id
            }
        }
        payPalItem.push(itemObj); 
    }

    return payPalItem;
  }

  getPayPalValue() {
    let res: number = 0;
    let total: number = 0;

    this.getCartFromLocalStorage();
    if(this.cart !== null) {
        for (const item of this.cart) {
             res = item.precio * 1;
             total = res + total;
        }
        
    }

    return total;
  }

  emptyCart() {
    this.cart = [];
    localStorage.removeItem('cart');
  }
  
  showDialog() {
    this.display = true;
  }
  displayInfoDialog(creditImage:string) {
    this.displayInfo = true;
    this.credit_image = creditImage != undefined ? creditImage : 'creditos_vuelta.jpg';  
  }

  private initConfig(): void {
    this.payPalConfig = {
      currency: 'MXN',
      clientId: environment.payPalClientId,
      createOrderOnClient: (data) => <ICreateOrderRequest> {
        intent: 'CAPTURE',
        purchase_units: [{
          amount: {
            currency_code: 'MXN',
            value: this.getPayPalValue().toString(),
            breakdown: {
              item_total: {
                currency_code: 'MXN',
                value: this.getPayPalValue().toString()
              }
            }
          },
          items: this.getItemList()
        }]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical'
      },
      onApprove: async (data, actions) => {
        this.spinner.show();
        
        console.log('onApprove - transaction was approved, but not authorized', data, actions);
        actions.order.get().then( details => {
          console.log('onApprove - you can get full order details inside onApprove: ', details);

        });
        
     
      },
      onClientAuthorization: async (data) => {
        console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
        const response  = await this._postService.setPayPalPurchase(this.token, data).toPromise();   
        this.items      = data.purchase_units[0].items;
        this.message    = response.message;

        this.emptyCart();
        this.spinner.hide();
        this.showDialog();   
      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);

      },
      onError: err => {
        console.log('OnError', err);
      },
      onClick: (data, actions) => {
        console.log('onClick', data, actions);
      },
    };
  }

}
