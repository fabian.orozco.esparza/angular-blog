import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { global } from '../../services/global';
import { ComprasService } from '../../services/compras.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Product } from '../../models/products';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-mis-compras',
  templateUrl: './mis-compras.component.html',
  styleUrls: ['./mis-compras.component.css'],
 providers: [ComprasService, UserService]
})
export class MisComprasComponent implements OnInit {
  private token: string;
  private identity: any;
  public url: string;
  public purhcasedDecks: Product[] = [];
  public deckImgs: any;
  public page_title: string = "Mis compras";
  public display: boolean = false;
  credits_image: string = "";
  public precio: number = 0;
  public nombreDeck: string = "";
  public descripcionDeck: string = "";

  constructor( private comprasService: ComprasService, private userService: UserService,   private spinner: NgxSpinnerService, private  http: HttpClient ) {
    this.url      = global.url;
    this.token    = userService.getToken();
    this.identity = userService.getIdentity();
  }

   async ngOnInit() {
    this.getCompras();
  }
  
  getCompras() {
    this.spinner.show();
    this.comprasService.getCompras(this.token).subscribe(response => {
      this.purhcasedDecks = response;
      console.log(this.purhcasedDecks);
      
      this.deckImgs = this.getDeckImgs();
      this.spinner.hide();
    });
  }

  getDeckImgs() {
    let imgArray: any[] = [];
    for (const item of this.purhcasedDecks) {
          imgArray.push(item.image);
    }
    return imgArray;
  }

  getDeckZipFile(fileName: string) {
    const obj = {
      purchasedDeck: fileName
    }
    console.log(obj);
    this.comprasService.downLoadZipFile(this.token, obj).subscribe(response => {
      this.downLoadFile(response, "application/octet-stream", fileName)
    });
  
    
   
  }

  downLoadFile(data: any, type: string, fileName: string) {
    fileName = fileName.replace('.png', '.zip');
    let blob = new Blob([data], { type: type});
    let downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(blob);
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
    
  }

  displayModal(deck: any): void {
    this.precio = deck.precio;
    this.nombreDeck = deck.nombre_deck;
    this.descripcionDeck = deck.descripcion;
    console.log(deck);
    
    this.credits_image = deck.imagen_creditos;
    this.display = true;
  }
}
