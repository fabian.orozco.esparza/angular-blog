import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { PostService } from '../../services/post.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService]
})
export class PostDetailComponent implements OnInit {
  public post: Post;

  constructor(
    private _postService: PostService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getPost();

  }

  getPost(){
    //Sacar el id del post de la url
    this._route.params.subscribe(params => {
      let id = +params['id']; // se le adjunta el símbolo mas al principio para convertir el string a entero

      //Peticion Ajax para sacar los datos del post
      this._postService.getPost(id).subscribe(
        response => {
            if(response.status == "success"){
                this.post = response.post;

                console.log(this.post);

            }else{
              

            }

        },
        error => {
          console.log(error);
          this._router.navigate(['/inicio']);
        }
      );

    });


  }

}
