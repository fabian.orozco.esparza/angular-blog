import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TecMonterreyComponent } from './tec-monterrey.component';

describe('TecMonterreyComponent', () => {
  let component: TecMonterreyComponent;
  let fixture: ComponentFixture<TecMonterreyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TecMonterreyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TecMonterreyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
