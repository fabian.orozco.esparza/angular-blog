import { Component, OnInit } from '@angular/core';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]

})
export class RegisterComponent implements OnInit {
  public page_title: string;
  public user: User;
  public status: string;
  errorMsg: string = "";
  showModal:boolean = false;

  constructor(
    private _userService: UserService,
    private spinner: NgxSpinnerService
  ) { 
    this.page_title = 'Registrate';
    this.user = new User(1, '', '', 'ROlE_USER', '', '','','');

  }

  ngOnInit() {
    
    //console.log(this._userService.test);
  }
 
  onSubmit(form){
    this.spinner.show();
    this.errorMsg = "";
    this._userService.register(this.user).subscribe(
      response => {
        if(response.status == "success") {
          this.status = response.status;
          form.reset();

        }else{
          this.status = 'error';
        }

        this.spinner.hide();
        this.showModal = true;
      }, 
      error => {
        this.status = 'error'
        console.log("entra else");
          
          if(error.hasOwnProperty('error')) {
          console.log("entra if");
              console.log(error.error.errors.email[0]);
              
              this.errorMsg = error.error.errors.email[0] ? "Esta cuanta ya ha sido registrada" : '';
          }
        console.log(<any>error)
        this.spinner.hide();
      }
    );
    
  }

  

}
