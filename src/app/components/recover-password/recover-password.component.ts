import { Component, OnInit } from '@angular/core';
import { PassRecoverInfo } from 'src/app/models/passRecover';
import { UserService } from 'src/app/services/user.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css'],
  providers: [UserService]
})
export class RecoverPasswordComponent implements OnInit {

  //creat variables para la nueva contraseña y el token
  passInfo: PassRecoverInfo;
  displayConfirmationModal: boolean = false;
  genericMesagge: string;
  modalTitle: string;
  icon: string;
  btnStyle: string;
  //al obtener la info comparar las dos contraseñasy validar
  //si todo bien, el usuario puede enviar su nueva contraseña
  //crear servicio para cambiar contraseña enviando la nueva contraseña y el token

  constructor(private _userService: UserService, private _router: Router, private _route: ActivatedRoute, private spinner: NgxSpinnerService) { 
    this.passInfo = {
      token: "",
      password: "",
      reEnterPass: ""
    };
  }

  ngOnInit(): void {
    
    this.getTokenFromUrl();
    this.redirectIfNoToken(this.passInfo.token);

  }

  getTokenFromUrl() {
    
    const queryString = window.location.search;
    if(queryString) {
      const urlParams = new URLSearchParams(queryString);
      const token = urlParams.get('idToken'); 

      this.passInfo.token = token;
    }
    
  }
  redirectIfNoToken(token) {
    if(token == undefined || token == "" || token == null) { 
      // Redirección a inicio
      this._router.navigate(['login']); 
    }

  }

  submitForm() {
    this.spinner.show();
    this._userService.updatePassword(this.passInfo).subscribe(
      response => {

          switch (response.code) {
            case 200:
                this.genericMesagge = response.message
                this.modalTitle = "Éxito!";
                this.icon = "fa fa-check-circle fa-2x";
                this.btnStyle = "background: green;";

            break;

            case 400:
                this.genericMesagge = response.message;
                this.modalTitle = "Ooops...";
                this.icon = "fa fa-exclamation-triangle fa-2x";
                this.btnStyle = "background: orange;";
                
            break;
              
            case 403:
                this.genericMesagge = response.message;
                this.modalTitle = "Ooops...";
                this.icon = "fa fa-exclamation-triangle fa-2x";
                this.btnStyle = "background: orange;";
            break;
            
            default:
              break;

          }
          this.spinner.hide();
          this.displayConfirmationModal = true;
        }
    )


  }

  hideConfirmationModal() {
    this.displayConfirmationModal = false;
    this._router.navigate(['login']);
  }

}
