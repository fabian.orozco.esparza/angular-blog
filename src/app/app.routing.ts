// IMPORTS NECESARIOS
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORTAR COMPONENTES
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { CategoryNewComponent } from './components/category-new/category-new.component';
import { PostNewComponent } from './components/post-new/post-new.component';
import {PostDetailComponent} from './components/post-detail/post-detail.component';
import {PresentationPageComponent} from './presentation-page/presentation-page.component';
import { MisComprasComponent } from './components/mis-compras/mis-compras.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { Route } from '@angular/compiler/src/core';
import { TecMonterreyComponent } from './components/tec-monterrey/tec-monterrey.component';

// DEFINIR RUTAS
const appRoutes: Routes = [
    { path: '', component: LandingPageComponent },
    { path: 'landing', component: LandingPageComponent },
    { path: 'registro', component: RegisterComponent },
    { path: 'login', component: PresentationPageComponent },
    { path: 'recover-password', component: RecoverPasswordComponent},
    { path: 'inicio', component: HomeComponent },
    { path: 'mis-compras', component: MisComprasComponent },
    { path: 'ajustes', component: UserEditComponent },
    { path: 'crear-categoria', component: CategoryNewComponent },
    { path: 'crear-entrada', component: PostNewComponent },
    { path: 'entrada/:id', component: PostDetailComponent },
    { path: 'logout/:sure', component: LoginComponent },
    { path: 'tec-monterrey', component: TecMonterreyComponent },
    {path: '**', component: ErrorComponent}, //La ruta con ** tiene que estar siempre al final
];

// EXPORTAR CONFIGURACIÓN
export const appRoutingProviders: any[]     = [];
export const routing: ModuleWithProviders<Route>   =  RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' });