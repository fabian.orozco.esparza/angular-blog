export interface Product {
    artista?:string;
    created_at?:string;
    deleted_at?:string;
    description?:string;
    id?:number;
    image?:string;
    nombre_deck?:string;
    precio?:number;
    qty?:number
    uid?:number;
    updated_at?:string;
}