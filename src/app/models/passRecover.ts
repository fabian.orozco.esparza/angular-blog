export interface PassRecoverInfo {
    token?: string;
    password?: string;
    reEnterPass?: string;
}