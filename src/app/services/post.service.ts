import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../models/post';
import {global} from './global';

@Injectable()
export class PostService{
    public url: string;

    constructor(private  _http: HttpClient){
        this.url = global.url;

    }

    redeem_code(token, code): Observable<any> {
        
        const json = JSON.stringify(code);
        const params = 'json=' + json;
        const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                         .set('Authorization', token);

        return this._http.post(this.url + 'code/redeem', params, {headers: headers} );
    }
    create(token, post): Observable<any> {
        let json = JSON.stringify(post);
        let params = 'json=' + json;
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                       .set('Authorization', token);

        return this._http.post(this.url + "post", params, {headers:headers});
    }

    getPosts(): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this._http.get(this.url + 'post', {headers: headers});
    }

    getCards(token): Observable<any>{
        
        let json = JSON.stringify(token);
        let params = 'json=' + json;
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization', token);

        return this._http.post(this.url + 'allcards', params, {headers: headers});
    }

    getCardsNoLogin(): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.get(this.url + 'allcardsNoLogin', {headers: headers});
    }

    getInstitutionalEvent(): Observable<any> {
        let headers = new HttpHeaders().set('Content-type', 'application/json');

        return this._http.get(this.url + 'getInstitutionalEvents', { headers: headers });
    }

    getPost(id): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this._http.get(this.url + 'post/'+id, { headers: headers });
    }

    setPayPalPurchase(token, data): Observable<any> {
        let json    = JSON.stringify(data);
        let params  = 'json=' + json;
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', token);
        

        return this._http.post(this.url + 'setNewPayment', params, { headers: headers });
        
    }
}