import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {global} from './global';
import { promise } from 'selenium-webdriver';

@Injectable()
export class UserService{
    public url: string;
    public identity;
    public token;

    constructor(public _http: HttpClient){
        this.url = global.url;

    }

    test(){
        return "Hola mundo desde un servicio"
    }

    register(user): Observable<any>{
        let json = JSON.stringify(user);
        let params = 'json='+json;

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');


        return this._http.post(this.url+'register', params, {headers:headers});
    }

    signup(user, getToken = null): Observable<any>{
        if(getToken != null){
            user.getToken = 'true';
        }

        let json = JSON.stringify(user);
        let params = 'json='+json;
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this._http.post(this.url+'login', params, {headers: headers});
    }

    recoverPassword(email:string): Observable<any> {
        let json = JSON.stringify(email);
        let params = 'json=' + json;
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this._http.post(this.url + 'recoverpassword', params, {headers: headers});
    }

    updatePassword(passInfo: any): Observable<any> {
        let json = JSON.stringify(passInfo);
        let params = "json="+json;

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this._http.put(this.url + 'user/updatePassword', params, {headers: headers});
    }
    update(token, user): Observable<any>{
        let json = JSON.stringify(user);
        let params = "json="+json;

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                       .set('Authorization', token);

        return this._http.put(this.url + 'user/update', params, {headers: headers});

    }
    getIdentity(){
        let identity = JSON.parse(localStorage.getItem('identity'));

        if(identity && identity != "undefined"){
            this.identity = identity;
        }else{
            this.identity = null;
        }

        return this.identity;
    }

    getToken(){
        let token = localStorage.getItem('token');

        (token != "undefined") ? this.token = token : this.token = null;

        return this.token;
    }
}
