export var global = {
    /* url for production */
    url: 'https://angular-blog-gusml.ondigitalocean.app/api/api/'

    /* url for stg */
    //url: 'api/api/'

    /* url for dev */
    //url: 'http://api-rest-laravel.com.test/api/'
}