import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { global } from './global';
import { Product } from '../models/products';


@Injectable({
  providedIn: 'root'
})
export class ComprasService {
  private url: string;
  public identity: any;
  public token: any;

  constructor( private http: HttpClient ) { 
    this.url = global.url;  
  }

  getCompras(token: string):Observable <Product[]> {  
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                    .set('Authorization', token);


    return this.http.get<Product[]>(this.url+'mis-compras', {headers:headers});
  }

  downLoadZipFile(token: string, zipFile: object) {
    let json    = JSON.stringify(zipFile);
    let params  = 'json=' + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', token);
    

    return this.http.post(this.url + 'downloadZip', params, { headers: headers, responseType:'arraybuffer' });
  }
}
