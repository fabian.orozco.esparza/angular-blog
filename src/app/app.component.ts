import { CategoryService } from './services/category.service';
import { Component, OnInit, DoCheck } from '@angular/core';
import {UserService} from './services/user.service';
import {global} from './services/global';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, CategoryService]

})
export class AppComponent implements OnInit, DoCheck {
  title = 'blog-angular';
  public identity;
  public token;
  public url;
  public categories;
  public isLoggedIn: boolean;

  constructor(public _userService: UserService, private _categoryService: CategoryService, private router: Router) {
    this.url = global.url;
    this.loadUser();
    // this.isLoggedIn = this.loadUser();
    // if (!this.isLoggedIn) {
    //   router.navigate(['registro']);
    // }
  }

  ngOnInit() {
    console.log('web app cargada: ');

    //this.getCategories();
  }

  ngDoCheck(){
    this.loadUser();
  }

  loadUser(){
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    let isLoggedIn: boolean;

    // return isLoggedIn = ((this.identity !== '' && this.identity !== null) && (this.token !== '' && this.token !== null) ? true : false);
  }

  getCategories(){
    this._categoryService.getCategories().subscribe(
      response => {
        if(response.status == 'success'){
          this.categories = response.categories;
          
        } 
        
      },
      error => {
        console.log(error);
      }
    );
  }
}
