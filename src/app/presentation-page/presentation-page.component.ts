import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'presentation-page-login',
  templateUrl: './presentation-page.component.html',
  styleUrls: ['./presentation-page.component.css']
})
export class PresentationPageComponent implements OnInit {
    public page_title: string;
    public user: User;
    public status: string;
    public token;
    public identity;
    public errorMsg: string = ""
    btnDisable = false;
    public verifyMsg:string = "";
    displayPassRecModal: boolean = false;
    genericMesagge: string;
    displayConfirmationModal: boolean = false;
    icon: string;
    btnStyle: string;
    modalTitle:string;

  constructor(private _userService: UserService, private _router: Router, private _route: ActivatedRoute, private spinner: NgxSpinnerService) { 
    this.page_title = 'Identifícate';
    this.user = new User(1, '', '', 'ROLE_USER', '', '','','');
  }

  ngOnInit() {
    this.displayPassRecModal = false;
    this.displayConfirmationModal = false;
    this.getVerificationStatus();
    //Se ejecuta siempre y cierra sesión solo cuando le llega el parámetro sure por la url
    this.logout();
  
  }
  getVerificationStatus() {
    this.errorMsg = "";
    const queryString = window.location.search;
    if(queryString) {
      const urlParams = new URLSearchParams(queryString);
      const status = urlParams.get('status'); 
      const code = urlParams.get('code'); 
      const mergeVars = status+code;
  
      switch(mergeVars) {
        case 'verified100': 
                this.verifyMsg = "El usuario ya ha sido verificado."
        break
  
        case 'error404': 
                this.verifyMsg = "Hubo un error al tratar de verificar al usuario, el token es incorrecto o el usuario no existe.";
        break

        case 'success200': 
                this.verifyMsg = "Usuario verificado correctamente";
        break
  
        default:
            this.verifyMsg = "";
        break;
      }
    }
  }

  onSubmit(form){
    this.errorMsg = "";
    this.spinner.show();
    this._userService.signup(this.user).subscribe(
      response => {
        // TOKEN
        if(response.status != 'error'){
        
          this.status = 'success';
          this.token = response;

          // OBJETO USUARIO IDENTIFICADO
          this._userService.signup(this.user, true).subscribe(
            response => {    
              if(response.status != 'error') {
                this.identity = response;
                // PERSISTIR DATOS USUARIO IDENTIFICADO
                localStorage.setItem('token', this.token);
                localStorage.setItem('identity', JSON.stringify(this.identity));
                this.spinner.hide();
                // Redirección a inicio
                this._router.navigate(['inicio']);
              } else {
                this.validateErrorResponse(response);
                this.spinner.hide();
              }
            },
            error => {
              this.status = 'error';
              console.log(<any>error);
              this.spinner.hide();
            }
          );

        } else {
          this.validateErrorResponse(response);
          this.status = 'error';
          this.spinner.hide();
        }

      },
      error => {
        this.status = 'error';
        console.log(<any>error);

      }
    )
  }

  validateErrorResponse(response: any) {
    if (response.hasOwnProperty('code')) {
      switch (response.code) {
        case 404:
            this.errorMsg = response.message;
          break;

        case 400:  
            this.errorMsg = response.errors;
            break;
        default:
            this.errorMsg = "Ha ocurrido un error, vuelve a intentarlo mas tarde";
          break;
      }
    }
  }

  logout(){
    this._route.params.subscribe(params => {
        let logout = +params['sure'];
        console.log(logout);
        if(logout == 1){
          localStorage.removeItem('identity');
          localStorage.removeItem('token');

          this.identity = null;
          this.token    = null;

          //Redirección a inicio
          this._router.navigate(['inicio']);
        }
    });
  }

  showPassRecModal() {
    this.displayPassRecModal = true;
  }

  async passwordRecover() {
    this.displayPassRecModal = false;
    this.spinner.show();
    let response = await this._userService.recoverPassword(this.user.email).toPromise();

    
    switch (response.code) {
          
      case 200:
        this.genericMesagge = response.message;
        this.modalTitle = "Revista tu correo!";
        this.icon = "fa fa-check-circle fa-2x";
        this.btnStyle = "background: green;";
      break;

      case 400:
        this.genericMesagge = response.message;
        this.modalTitle = "Ooops...";
        this.icon = "fa fa-exclamation-triangle fa-2x";
        this.btnStyle = "background: orange;";
       
      break;
      
      case 500:
        this.genericMesagge = response.message;
        this.modalTitle = "Ooops...";
        this.icon = "fa fa-exclamation-triangle fa-2x";
        this.btnStyle = "background: orange;";

      break;
    
      default:
        break;
    }
      this.spinner.hide();
      this.displayConfirmationModal = true;
        
  }

  hideConfirmationModal() {
    this.displayConfirmationModal = false;
    this.genericMesagge = "";
    this.btnStyle = "";
    
  }

}
