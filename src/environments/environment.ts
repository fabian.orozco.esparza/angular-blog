// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  payPalClientId: 'AeKsz9TxufMPDDa_EaJqpm1jLecjP-5TbJogqCjC5QGNmcQ7kCi-n8coUVrK6s2KpRjUJITJO_iMux_v', //prod
  //payPalClientId: 'Aclg5gbC3SL2oHlg5hbrKf1wQTNHAnh3P87qJP-94_2RzL5WT7JbUlI6ia3FNSou48D5n2jZ94TVmNxH', //dev
  //payPalClientIdInst: 'Aeq35kfpduVvrcKoWJJLFoZlZ5w0b9WfMLr4NdPLA72vAC2Ft8ij6MshFVn7xnsR9f8sA9TQcQ1T9YUJ'  //se registró otra app en developer paypal para el segundo boton de paypal
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
